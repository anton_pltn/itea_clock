<?php

namespace getTimeService;


class getTimeService{
    // public $zone;

    // public function __construct($zone)
    // {
    //     date_default_timezone_set($zone);
    //     return $date = date('d-m-y h:i:s');   
    // }

    public function get_date($zone)
    {
        date_default_timezone_set($zone);
        return $date = date('d-m-y h:i:s');   
    }

}