<?php
//standart php library
spl_autoload_register(function($className) {

    $dirs = explode('\\', $className);

    $path = '';

    foreach ($dirs as $k => $dir) {
        if ($k === array_key_last($dirs)) {
            $path .= sprintf('%s.php', $dir);
            continue;
        }

        if ($k !== array_key_first($dirs) ) {
            $path .= lcfirst($dir).DIRECTORY_SEPARATOR;
        }

    }

    require_once sprintf('%s', $path);
});